student_list = ['belle', 'ariel', 'aurora', 'anna', 'tiana', 'merida']
student_grade = [60, 80, 95, 85, 89, 97]

def continue_to_menu():
	print(input('\nPress ENTER to continue'))
	main_menu()

def main_menu():
	menu = """
	[program begins]
	Menu: 
	1. Add a new student & grade
	2. Print all students
	3. Print all grades
	4. Lookup a student's grade
	5. Find student with highest grade
	6. Find student with lowest grade
	7. Generate graph
	8. Quit
	"""
	print(menu)

main_menu()

student_list.append('Hannah')
student_grade.append(80)

print(student_list)
print('')
print(student_grade)
print('')

import pandas as pd
df = pd.DataFrame(columns = ['Name', 'Grade'], index = [1, 2, 3, 4, 5, 6, 7])
df['Name'] = student_list
df['Grade'] = student_grade
print(df)

belle_grade = df.loc[df['Name'] == 'belle', 'Grade'].iloc[0]
print(belle_grade)
print('')

highest_grade = df.loc[df['Grade'].idxmax()].iloc[0]
print(highest_grade)
print('')

lowest_grade = df.loc[df['Grade'].idxmin()].iloc[0]
print(lowest_grade)
print('')

import matplotlib.pyplot as plt
plt.bar(df['Name'], df['Grade'])
plt.xlabel('Name')
plt.ylabel('Grade')
plt.title('Ages of Persons')
plt.show()